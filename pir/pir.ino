/*
 * Read from a PIR.
 * Store measurements in a moving window, raise alarm when
 * the number of positive indications in the window reach
 * a given threshold, then reset to the initial conditions.
 * 
 * Circuitry:
 * - connect led_pin to LED to resistor to GND
 * - connect pir_pin to input from a PIR
 * 
 * Copyright (c) 2017 C. Cicconetti
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// configuration
const unsigned int maxCnt = 50; // moving window size
const unsigned int threshold = 40; // to raise alarm
const unsigned int sleep_time = 200; // ms
const int led_pin = 6;
const int pir_pin = 7;
const bool print_ser = false; // debug stuff

// internal data structure
int measurements[maxCnt]; // circular buffer
int cur_ndx = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication, if needed
  if (print_ser) { Serial.begin(9600); }
  
  // pin setup
  pinMode(pir_pin, INPUT);
  pinMode(led_pin, OUTPUT);
  
  // start from initial conditions
  reset();
}

// reset to initial conditions
void reset() {
  for (int i = 0 ; i < maxCnt ; i++) {
    measurements[i] = LOW;
  }
  digitalWrite(led_pin, LOW);
}

// called to raise an alarm
void alarm() {
  digitalWrite(led_pin, HIGH);
  delay(5000);
  reset();
}

// main loop
void loop() {
  // int buttonState = digitalRead(pushButton);
  measurements[cur_ndx] = digitalRead(pir_pin);
  cur_ndx = cur_ndx < (maxCnt - 1) ? (cur_ndx + 1) : 0;

  int num_alarms = 0;
  for (int i = 0 ; i < maxCnt ; i++) {
    if (measurements[i] == HIGH) {
      num_alarms++;
    }
  }

  if (print_ser) { Serial.println(num_alarms); }
  
  if (num_alarms >= threshold) {
    alarm();
  } else {
    delay(sleep_time);
  }
}

