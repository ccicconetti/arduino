/*
 LiquidCrystal Library - Hello World

 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.

 This sketch prints "Hello World!" to the LCD
 and shows the time.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int a = 1;
int b = 1;
int num_it = 0;
bool ser_print = false;
// int led_pin = 13; // on-board led
int led_pin = 6;

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Fibonacci seq:");
  // initialize serial communication at 9600 bits per second:
  if (ser_print) { Serial.begin(9600); }
  // setup the digital output to drive the led
  pinMode(led_pin,OUTPUT);
  digitalWrite(led_pin,LOW);
}

void loop() {
  int n = 0;
  if (num_it == 0) {
    n = a;
  } else if (num_it == 1) {
    n = b;
  } else {
    n = a + b;

    if (n < 0) {
      lcd.setCursor(0, 1);
      lcd.print("OVERFLOW!");
      delay(1000);
      lcd.setCursor(0, 1);
      lcd.print("                ");
      if (ser_print) { 
        Serial.println("OVERFLOW!");
      }
      delay(1000);
      a = 1;
      b = 1;
      n = 1;
      num_it = -1;
    } else {
      a = b;
      b = n;
    }
  }
  num_it++;
  lcd.setCursor(0, 1);
  lcd.print("#");
  lcd.setCursor(1, 1);
  lcd.print(num_it);
  int len = 1;
  if (num_it >= 10) {
    len = 2;
  }
  lcd.setCursor(1 + len, 1);
  lcd.print(" ");
  lcd.setCursor(2 + len, 1);
  lcd.print(n);
  if (ser_print) {
    Serial.print("#");
    Serial.print(num_it);
    Serial.print(" ");
    Serial.println(n);
  }
  digitalWrite(led_pin,(num_it % 2 == 0) ? HIGH : LOW);
  delay(1000);
}

