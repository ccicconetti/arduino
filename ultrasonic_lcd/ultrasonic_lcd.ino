/*
 * Measure distance with an ultrasonic HC-SR04 print value
 * to a 16x2 LCD. Switch on a led if the distance is
 * below a critical threshold twice in a moving window.
 * 
 * Circuit:
 * - LCD RS pin to digital pin 12
 * - LCD Enable pin to digital pin 11
 * - LCD D4 pin to digital pin 5
 * - LCD D5 pin to digital pin 4
 * - LCD D6 pin to digital pin 3
 * - LCD D7 pin to digital pin 2
 * - LCD R/W pin to ground
 * - LCD VSS pin to ground
 * - LCD VCC pin to 5V
 * - 10K resistor:
 *   ends to +5V and ground
 *   wiper to LCD VO pin (pin 3)
 * - led_pin to LED to resistor to GND
 * - echo_pin to the echo connector of HC-SR04
 * - trig_pin to the trigger connector of HC-SR04
 * 
 * Libraries:
 * - LiquidCrystal
 * 
 * This code is a modification of the HelloWorld program
 * shipped with the LiquidCrystal library contributed by
 * David A. Mellis, Limor Fried, and Tom Igoe.
 * 
 * The HC-SR04 part comes from code by Dejan Nedelkovski:
 * http://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
 * 
 * The library and original programs are in the public domain.
 */

#include <LiquidCrystal.h>

// initialize the LCD library with the right interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// alarm detection configuration
const unsigned int max_cnt = 10; // moving window size
const unsigned int threshold = 100; // in cm
const unsigned int num_flags = 2; // two occurrences to raise alarm
const unsigned int sleep_time = 200; // ms
const int led_pin = 6;
const int echo_pin = 9;
const int trig_pin = 8;
const bool print_ser = true; // debug stuff

// internal data structure
int measurements[max_cnt]; // circular buffer
unsigned int cur_ndx = 0;
int distance = -1;

// the setup routine runs once when you press reset:
void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("distance (cm):");

  // initialize serial communication, if needed
  if (print_ser) { Serial.begin(9600); }
  
  // pin setup
  pinMode(echo_pin, INPUT);
  pinMode(trig_pin, OUTPUT);
  pinMode(led_pin, OUTPUT);
  
  // start from initial conditions
  reset();
}

// reset to initial conditions
void reset() {
  for (int i = 0 ; i < max_cnt ; i++) {
    measurements[i] = 32767;
  }
  digitalWrite(led_pin, LOW);
}

// called to raise an alarm
void alarm() {
  digitalWrite(led_pin, HIGH);
  delay(5000);
  reset();
}

// print alarm info to the LCD
void lcd_print() {
  if (print_ser) { Serial.println(distance); }
  
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print(distance);
}

// main loop
void loop() {
  // clear the trigPin
  digitalWrite(trig_pin, LOW);
  delayMicroseconds(2);
  
  // set the trig_pin on HIGH state for 10 us
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_pin, LOW);
  
  // read the echo_pin, returns the sound wave travel time in microseconds
  long duration = pulseIn(echo_pin, HIGH);
  
  // calculating the distance
  distance = duration * 0.034 / 2;

  // print to LCD
  lcd_print();

  // store measurement in memory ando check for alarms
  measurements[cur_ndx] = distance;
  cur_ndx = cur_ndx < (max_cnt - 1) ? (cur_ndx + 1) : 0;

  int num_alarms = 0;
  for (int i = 0 ; i < max_cnt ; i++) {
    if (measurements[i] <= threshold) {
      num_alarms++;
    }
  }

  if (num_alarms >= num_flags) {
    alarm();
  } else {
    delay(sleep_time);
  }
}

