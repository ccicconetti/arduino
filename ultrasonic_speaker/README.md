### Ultrasonic (with speaker)

Pictures of the board and sensor in a home-made Lego enclosure:

![full view](full_view.jpg)
![sensor](sensor.jpg)
![board](board.jpg)
