/*
 * Measure distance with an ultrasonic HC-SR04.
 * Play a melody on a speaker when the distance is
 * below a critical threshold for a given number of 
 * occurrences in a moving window of configurable size.
 * Waits a delay between consecutive sensing cycles.
 * 
 * When started the sensor is idle: it is set to active
 * by pushing a button; it can be brought back to an
 * idle state by pushing the button again.
 * 
 * Circuit:
 * - echo_pin to the echo connector of HC-SR04
 * - trig_pin to the trigger connector of HC-SR04
 * - spk_pin to the a 100 Ohm resistor, the other
 *   end goes to speaker (+)
 * - speaker (-) to GND
 * - button_pin to a 10 kOhm resistor whose other end
 *   goes go GND and to a push button, whose other leg
 *   goes to VCC
 * 
 * Libraries:
 * - pitches.h from Arduino library
 * 
 * This code is a mix and match of the toneMelody program
 * by Tom Igoe (toneMelody) and the the ultrasonic example
 * by Dejan Nedelkovski, both in  the public domain.
 */

/*************************************************
 * Public Constants
 *************************************************/

#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

// alarm detection configuration
const unsigned int max_cnt = 2; // moving window size
const unsigned int threshold = 90; // in cm
const unsigned int num_flags = 1; // two occurrences to raise alarm
const unsigned int sleep_time = 100; // ms
const int button_pin = 2;
const int led_pin = 13;
const int echo_pin = 9;
const int trig_pin = 8;
const int spk_pin = 7;
const bool print_ser = false; // debug stuff
const int melody[] = {
  NOTE_A3, NOTE_A3, NOTE_A3, NOTE_F3, NOTE_C4, NOTE_A3, NOTE_F3, NOTE_C4, NOTE_A3
};
const int note_durations[] = { // in ms
  500, 500, 500, 250, 250, 500, 250, 250, 1000
};

// internal data structure
int measurements[max_cnt]; // circular buffer
unsigned int cur_ndx = 0;
int distance = -1;
bool idle = true;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication, if needed
  if (print_ser) { Serial.begin(9600); }
  
  // pin setup
  pinMode(echo_pin, INPUT);
  pinMode(trig_pin, OUTPUT);
  pinMode(led_pin, OUTPUT);
  pinMode(button_pin, INPUT);
  
  // start from initial conditions
  reset();
}

// reset to initial conditions
void reset() {
  for (int i = 0 ; i < max_cnt ; i++) {
    measurements[i] = 32767;
  }
  digitalWrite(led_pin, LOW);
}

// raise an alarm
void alarm() {
  digitalWrite(led_pin, HIGH);
  imperial_march();
  reset();
}

// play the Imperial March melody from Star Wars
void imperial_march() {
  // iterate over the notes of the melody
  for (int i = 0; i < 9; i++) {
    tone(spk_pin, melody[i], note_durations[i]);

    // to distinguish the notes, set a minimum time between them
    // the note's duration + 30% seems to work well
    delay(note_durations[i] * 1.3);
    
    // stop the tone playing
    noTone(spk_pin);
  }
}

// main loop while the state is active
void active_loop() {
  // clear the trig_pin
  digitalWrite(trig_pin, LOW);
  delayMicroseconds(2);
  
  // set the trig_pin on HIGH state for 10 us
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_pin, LOW);
  
  // read the echo_pin, returns the sound wave travel time in microseconds
  const long duration = pulseIn(echo_pin, HIGH);
  
  // calculating the distance
  distance = duration * 0.034 / 2;

  if (print_ser) { Serial.println(distance); }

  // store measurement in memory ando check for alarms
  measurements[cur_ndx] = distance;
  cur_ndx = cur_ndx < (max_cnt - 1) ? (cur_ndx + 1) : 0;

  int num_alarms = 0;
  for (int i = 0 ; i < max_cnt ; i++) {
    if (measurements[i] <= threshold) {
      num_alarms++;
    }
  }

  if (num_alarms >= num_flags) {
    alarm();
  } else {
    digitalWrite(led_pin, HIGH);
    delay(sleep_time);
    digitalWrite(led_pin, LOW);
  }
}

// play a simple sequence of led blinking to confirm
// to the user that a command has been received
void change_state() {
  digitalWrite(led_pin, LOW);
  delay(1000);
  digitalWrite(led_pin, HIGH);
  delay(100);
  digitalWrite(led_pin, LOW);
  delay(100);
  digitalWrite(led_pin, HIGH);
  delay(100);
  digitalWrite(led_pin, LOW);
  delay(1000);
}

// main loop
void loop() {
  // read the state of the pushbutton value:
  int buttonState = digitalRead(button_pin);

  // if the state is idle then button pushing
  // means to make the device active
  if (idle) {
    if (buttonState == HIGH) {
      change_state();
      idle = false;
    }

  // otherwise, if the device is already active
  // then button pushing means make it idle
  } else {
    if (buttonState == HIGH) {
      change_state();
      idle = true;
    } else {
      active_loop();      
    }
  }
}

