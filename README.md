# Simple surveillance Arduino projects

This repository contains a few Arduino sketches to drive sensors and human interface devices related to presence detection.

Tested with Arduino Uno.

## Sketches

### PIR

Read from a PIR.  Store measurements in a moving window, raise alarm when the number of positive indications in the window reach a given threshold, then reset to the initial conditions.

### PIR (with LCD)

Same as above, but prints last two alarms to a 16x2 LCD display.

### Ultrasonic (with LCD)

Measure distance with an ultrasonic HC-SR04 print value to a 16x2 LCD. Switch on a led if the distance is below a critical threshold twice in a moving window.

### Ultrasonic (with speaker)

Measure distance with an ultrasonic HC-SR04.
Play the Imperial March from Star Wars on a speaker when the distance is below a critical threshold for a given number of occurrences in a moving window of configurable size.  Waits a delay between consecutive sensing cycles.
A push button allows the user to switch between idle and active mode.

### Fibonacci counter

Displays the sequence of Fibonacci on a 16x2 LCD display.

## License

This code is in the public domain.
Attribution to the respective authors of portions/libraries included in software are given in the sketches.

## Disclaimer

The sketches in this repository are provided for educational purposes only.
Full disclaimer [here](DISCLAIMER).
