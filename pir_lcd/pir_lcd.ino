/*
 * Read from a PIR, print last two alarms to a 16x2 LCD.
 * 
 * Store measurements in a moving window, raise alarm when
 * the number of positive indications in the window reach
 * a given threshold, then reset to the initial conditions.
 * 
 * When an alarm condition occurs a timestamp is printed.
 * 
 * Circuit:
 * - LCD RS pin to digital pin 12
 * - LCD Enable pin to digital pin 11
 * - LCD D4 pin to digital pin 5
 * - LCD D5 pin to digital pin 4
 * - LCD D6 pin to digital pin 3
 * - LCD D7 pin to digital pin 2
 * - LCD R/W pin to ground
 * - LCD VSS pin to ground
 * - LCD VCC pin to 5V
 * - 10K resistor:
 *   ends to +5V and ground
 *   wiper to LCD VO pin (pin 3)
 * - led_pin to LED to resistor to GND
 * - pir_pin to input from a PIR
 * 
 * Libraries:
 * - LiquidCrystal
 * 
 * This code is a modification of the HelloWorld program
 * shipped with the LiquidCrystal library contributed by
 * David A. Mellis, Limor Fried, and Tom Igoe.
 * The original library and program are in the public domain.
 */

#include <LiquidCrystal.h>

// initialize the LCD library with the right interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// alarm detection configuration
const unsigned int maxCnt = 50; // moving window size
const unsigned int threshold = 40; // to raise alarm
const unsigned int sleep_time = 200; // ms
const int led_pin = 6;
const int pir_pin = 7;
const bool print_ser = false; // debug stuff

// internal data structure
int measurements[maxCnt]; // circular buffer
unsigned int cur_ndx = 0;
unsigned int alarm_cnt = 0;
unsigned long alarm1_ts = 0;
unsigned long alarm2_ts = 0;

// the setup routine runs once when you press reset:
void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // initialize serial communication, if needed
  if (print_ser) { Serial.begin(9600); }
  
  // pin setup
  pinMode(pir_pin, INPUT);
  pinMode(led_pin, OUTPUT);
  
  // start from initial conditions
  reset();
}

// reset to initial conditions
void reset() {
  for (int i = 0 ; i < maxCnt ; i++) {
    measurements[i] = LOW;
  }
  digitalWrite(led_pin, LOW);
}

// called to raise an alarm
void alarm() {
  alarm_cnt++;
  digitalWrite(led_pin, HIGH);
  alarm1_ts = alarm2_ts;
  alarm2_ts = millis() / 60000UL; // minutes
  lcd_print();
  delay(5000);
  reset();
}

// print alarm info to the LCD
void lcd_print() {
  int next = 0;
  lcd.clear();
  
  lcd.setCursor(0, 0);
  next = 1 + lcd.print("#");
  lcd.setCursor(next, 0);
  next = next + 1 + lcd.print(alarm_cnt - 1);
  lcd.setCursor(next, 0);
  next = next + 1 + lcd.print(alarm1_ts);

  lcd.setCursor(0, 1);
  next = 1 + lcd.print("#");
  lcd.setCursor(next, 1);
  next = next + 1 + lcd.print(alarm_cnt);
  lcd.setCursor(next, 1);
  next = next + 1 + lcd.print(alarm2_ts);
}

// main loop
void loop() {
  // int buttonState = digitalRead(pushButton);
  measurements[cur_ndx] = digitalRead(pir_pin);
  cur_ndx = cur_ndx < (maxCnt - 1) ? (cur_ndx + 1) : 0;

  int num_alarms = 0;
  for (int i = 0 ; i < maxCnt ; i++) {
    if (measurements[i] == HIGH) {
      num_alarms++;
    }
  }

  if (print_ser) { Serial.println(num_alarms); }
  
  if (num_alarms >= threshold) {
    alarm();
  } else {
    delay(sleep_time);
  }
}

